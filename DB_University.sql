create database [DB_University]
go

USE [DB_University]
GO
/****** Object:  StoredProcedure [dbo].[sp_dosen_delete]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_dosen_delete]
as

select * from [dbo].[tbl_m_dosen]
GO
/****** Object:  StoredProcedure [dbo].[sp_dosen_insert]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_dosen_insert]
as

select * from [dbo].[tbl_m_dosen]
GO
/****** Object:  StoredProcedure [dbo].[sp_dosen_select]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_dosen_select]
as

select * from [dbo].[tbl_m_dosen]
GO
/****** Object:  StoredProcedure [dbo].[sp_dosen_update]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_dosen_update]
as

select * from [dbo].[tbl_m_dosen]
GO
/****** Object:  Table [dbo].[tbl_m_agama]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_m_agama](
	[id_agama_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_agama] [char](5) NULL,
	[deskripsi] [nvarchar](50) NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_tbl_m_agama] PRIMARY KEY CLUSTERED 
(
	[id_agama_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_m_dosen]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_m_dosen](
	[id_dosen_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_dosen] [char](5) NOT NULL,
	[nama_dosen] [nvarchar](100) NOT NULL,
	[id_jurusan_fk] [int] NOT NULL,
	[id_type_dosen_fk] [int] NOT NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_tbl_m_dosen] PRIMARY KEY CLUSTERED 
(
	[id_dosen_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_m_jurusan]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_m_jurusan](
	[id_jurusan_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_jurusan] [char](5) NOT NULL,
	[nama_jurusan] [nvarchar](50) NOT NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_tbl_m_jurusan] PRIMARY KEY CLUSTERED 
(
	[id_jurusan_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_m_mahasiswa]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_m_mahasiswa](
	[id_mahasiswa_pk] [bigint] IDENTITY(1,1) NOT NULL,
	[kode_mahasiswa] [char](5) NOT NULL,
	[nama_mahasiswa] [nvarchar](100) NOT NULL,
	[alamat] [nvarchar](200) NULL,
	[id_agama_fk] [int] NULL,
	[id_jurusan_fk] [int] NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
	[created_by1] [datetime] NULL,
	[createdby] [datetime] NULL,
	[createddate] [datetime] NULL,
 CONSTRAINT [PK_tbl_m_mahasiswa] PRIMARY KEY CLUSTERED 
(
	[id_mahasiswa_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_m_type_dosen]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_m_type_dosen](
	[id_type_dosen_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_type_dosen] [char](5) NOT NULL,
	[deskripsi] [nvarchar](50) NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_tbl_m_type_dosen] PRIMARY KEY CLUSTERED 
(
	[id_type_dosen_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_r_ujian]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_r_ujian](
	[id_ujian_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_ujian] [char](5) NULL,
	[nama_ujian] [nvarchar](50) NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_tbl_r_ujian] PRIMARY KEY CLUSTERED 
(
	[id_ujian_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_t_nilai]    Script Date: 12/11/2019 06.05.58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_nilai](
	[id_nilai_pk] [bigint] IDENTITY(1,1) NOT NULL,
	[id_mahasiswa_fk] [bigint] NULL,
	[id_ujian_fk] [int] NULL,
	[nilai] [decimal](18, 2) NULL,
	[is_active] [bit] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[updated_by] [nvarchar](50) NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_tbl_t_nilai] PRIMARY KEY CLUSTERED 
(
	[id_nilai_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_m_agama] ON 

GO
INSERT [dbo].[tbl_m_agama] ([id_agama_pk], [kode_agama], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'A0001', N'Islam', 1, N'adit', CAST(0x0000AB020160EF6F AS DateTime), N'adit', CAST(0x0000AB020160EF6F AS DateTime))
GO
INSERT [dbo].[tbl_m_agama] ([id_agama_pk], [kode_agama], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'A0002', N'Kristen', 1, N'adit', CAST(0x0000AB020161CC55 AS DateTime), N'adit', CAST(0x0000AB020161F881 AS DateTime))
GO
INSERT [dbo].[tbl_m_agama] ([id_agama_pk], [kode_agama], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'A0003', N'Khatolik', 1, N'adit', CAST(0x0000AB020163EF6A AS DateTime), N'adit', CAST(0x0000AB02016516EB AS DateTime))
GO
INSERT [dbo].[tbl_m_agama] ([id_agama_pk], [kode_agama], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'A0004', N'Budha', 1, N'adit', CAST(0x0000AB020164FFF2 AS DateTime), N'adit', CAST(0x0000AB020164FFF2 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_m_agama] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_m_dosen] ON 

GO
INSERT [dbo].[tbl_m_dosen] ([id_dosen_pk], [kode_dosen], [nama_dosen], [id_jurusan_fk], [id_type_dosen_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'D001 ', N'Prof. Dr. Retno Wahyuningsih', 1, 2, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_dosen] ([id_dosen_pk], [kode_dosen], [nama_dosen], [id_jurusan_fk], [id_type_dosen_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'D002 ', N'Prof. Roy M. Sutikno', 2, 1, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_dosen] ([id_dosen_pk], [kode_dosen], [nama_dosen], [id_jurusan_fk], [id_type_dosen_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'D003 ', N'Prof. Hendri A. Verburgh', 3, 2, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_dosen] ([id_dosen_pk], [kode_dosen], [nama_dosen], [id_jurusan_fk], [id_type_dosen_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'D004 ', N'Prof. Risma Suparwata', 4, 2, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_dosen] ([id_dosen_pk], [kode_dosen], [nama_dosen], [id_jurusan_fk], [id_type_dosen_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, N'D005 ', N'Prof. Amir Sjarifuddin Madjid, MM, MA', 5, 1, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_m_dosen] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_m_jurusan] ON 

GO
INSERT [dbo].[tbl_m_jurusan] ([id_jurusan_pk], [kode_jurusan], [nama_jurusan], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'J001 ', N'Teknik Informatika', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_jurusan] ([id_jurusan_pk], [kode_jurusan], [nama_jurusan], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'J002 ', N'Management Informatika', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_jurusan] ([id_jurusan_pk], [kode_jurusan], [nama_jurusan], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'J003 ', N'Sistem Informasi', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_jurusan] ([id_jurusan_pk], [kode_jurusan], [nama_jurusan], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'J004 ', N'Sistem Komputer', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_jurusan] ([id_jurusan_pk], [kode_jurusan], [nama_jurusan], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, N'J005 ', N'Komputer Akuntansi', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_m_jurusan] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_m_mahasiswa] ON 

GO
INSERT [dbo].[tbl_m_mahasiswa] ([id_mahasiswa_pk], [kode_mahasiswa], [nama_mahasiswa], [alamat], [id_agama_fk], [id_jurusan_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date], [created_by1], [createdby], [createddate]) VALUES (1, N'M001 ', N'Budi Gunawan', N'Jl. Mawar No 3 RT 05 Cicalengka, Bandung', 1, 1, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_m_mahasiswa] ([id_mahasiswa_pk], [kode_mahasiswa], [nama_mahasiswa], [alamat], [id_agama_fk], [id_jurusan_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date], [created_by1], [createdby], [createddate]) VALUES (2, N'M002 ', N'Rinto Raharjo', N'Jl. Kebagusan No. 33 RT04 RW06 Bandung', 1, 2, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_m_mahasiswa] ([id_mahasiswa_pk], [kode_mahasiswa], [nama_mahasiswa], [alamat], [id_agama_fk], [id_jurusan_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date], [created_by1], [createdby], [createddate]) VALUES (3, N'M003 ', N'Asep Saepudin', N'Jl. Sumatera No. 12 RT02 RW01, Ciamis', 1, 3, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_m_mahasiswa] ([id_mahasiswa_pk], [kode_mahasiswa], [nama_mahasiswa], [alamat], [id_agama_fk], [id_jurusan_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date], [created_by1], [createdby], [createddate]) VALUES (4, N'M004 ', N'M. Hafif Isbullah', N'Jl. Jawa No 01 RT01 RW01, Jakarta Pusat', 2, 1, 0, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_m_mahasiswa] ([id_mahasiswa_pk], [kode_mahasiswa], [nama_mahasiswa], [alamat], [id_agama_fk], [id_jurusan_fk], [is_active], [created_by], [created_date], [updated_by], [updated_date], [created_by1], [createdby], [createddate]) VALUES (5, N'M005 ', N'Cahyono', N'Jl. Niagara No. 54 RT01 RW09, Surabaya', 3, 2, 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_m_mahasiswa] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_m_type_dosen] ON 

GO
INSERT [dbo].[tbl_m_type_dosen] ([id_type_dosen_pk], [kode_type_dosen], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'T001 ', N'Tetap', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_type_dosen] ([id_type_dosen_pk], [kode_type_dosen], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'T002 ', N'Honorer', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_m_type_dosen] ([id_type_dosen_pk], [kode_type_dosen], [deskripsi], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'T003 ', N'Expertise', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_m_type_dosen] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_r_ujian] ON 

GO
INSERT [dbo].[tbl_r_ujian] ([id_ujian_pk], [kode_ujian], [nama_ujian], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'U001 ', N'Algoritma', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_r_ujian] ([id_ujian_pk], [kode_ujian], [nama_ujian], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'U002 ', N'Aljabar', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_r_ujian] ([id_ujian_pk], [kode_ujian], [nama_ujian], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'U003 ', N'Statistika', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_r_ujian] ([id_ujian_pk], [kode_ujian], [nama_ujian], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'U004 ', N'Etika Profesi', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_r_ujian] ([id_ujian_pk], [kode_ujian], [nama_ujian], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, N'U005 ', N'Bahasa Inggris', 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_r_ujian] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_t_nilai] ON 

GO
INSERT [dbo].[tbl_t_nilai] ([id_nilai_pk], [id_mahasiswa_fk], [id_ujian_fk], [nilai], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, 4, 1, CAST(90.00 AS Decimal(18, 2)), 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_t_nilai] ([id_nilai_pk], [id_mahasiswa_fk], [id_ujian_fk], [nilai], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, 1, 1, CAST(80.00 AS Decimal(18, 2)), 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_t_nilai] ([id_nilai_pk], [id_mahasiswa_fk], [id_ujian_fk], [nilai], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, 2, 3, CAST(85.00 AS Decimal(18, 2)), 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_t_nilai] ([id_nilai_pk], [id_mahasiswa_fk], [id_ujian_fk], [nilai], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, 4, 2, CAST(95.00 AS Decimal(18, 2)), 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
INSERT [dbo].[tbl_t_nilai] ([id_nilai_pk], [id_mahasiswa_fk], [id_ujian_fk], [nilai], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, 5, 5, CAST(70.00 AS Decimal(18, 2)), 1, N'adit', CAST(0x0000AAF600E3A532 AS DateTime), N'adit', CAST(0x0000AAF600E3A532 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_t_nilai] OFF
GO
