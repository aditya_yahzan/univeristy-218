﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Materi.University.ViewModel
{
    public class TypeDosenViewModel
    {
        public int id_type_dosen_pk { get; set; }
        public string kode_type_dosen { get; set; }
        public string deskripsi { get; set; }
        public bool is_active { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string updated_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    }
}
