﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Materi.University.ViewModel
{
    public class DosenViewModel
    {
        public int id_dosen_pk { get; set; }
        public string kode_dosen { get; set; }
        public string nama_dosen { get; set; }
        public int id_jurusan_fk { get; set; }
        public int id_type_dosen_fk { get; set; }
        public bool is_active { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string updated_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    }
}
